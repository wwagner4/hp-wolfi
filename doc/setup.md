## Set up the homepage

git clone
```shell
git clone https://gitlab.com/wwagner4/hp-wolfi.git
cd hp-wolfi
```
or
```shell
git pull
```

clone submodules
```shell
git submodule update --init --recursive
```
to start locally call 
```
cd helm && helm install hp hp-wolfi/
```

to stat it on entelijan
```
cd app/hp-wolfi/helm && helm install -f hp-wolfi/values-entelijan.yaml hp hp-wolfi/
```

to stop all call
```
cd app/hp-wolfi/helm && helm delete hp
```


homepage should be accessible as
```shell
http://localhost
```

