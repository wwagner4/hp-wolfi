# Howto setup a kubernetes cluster.

For each service define:
 * Define a Deployment
 * Define a Service, evtl. external Service
 * Define Ingress

## Define a Deployment
A deployment defines how to a docker image is 
used to start a container.

The main attributes are:
 * Name, to identify it in the cluster. E.g. hp-wolfi
 * Image name and tag from dockerhub. E.g. wwagner4/hp-wolfi:latest
 * Labele to link the container to other kubernetes objects. E.g. app: hp-wolfi 
 * The number of replicas

(Deployment Reference)[https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/deployment-v1/]


## Define a Service
The service defines how other containers can be
accessed from other containers or from the outside.

The main attributes are:
 * Name, to identify it in the cluster. E.g. hp-wolfi
 * Image name and tag from dockerhub. E.g. wwagner4/hp-wolfi:latest
 * Labele to link the container to other kubernetes objects. E.g. app: hp-wolfi 
 
(Deployment Reference)[https://kubernetes.io/docs/reference/kubernetes-api/service-resources/service-v1/]


## Define an external Service
A service that might be accessed from the outside. 

The main attributes are:
 * The same as for a service
 * The service type: NodePort
 * Port for access: nodePort, range 30000 - 32000 
 
(Deployment Reference)[https://kubernetes.io/docs/reference/kubernetes-api/service-resources/service-v1/]

microk8n:  
Enable the host-access plugin and then you can access the service on the ip 10.0.1.1



## Define an Ingress

TODO
