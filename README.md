# My homepage

see http://entelijan.net

## TODOs

- Add all working projects to kubernetes
- new family images
- new wolfi image
- all docker images to dockerhub with tag 1.0
- gutenberg
 -- Default values for env variable
 -- auto create epub directory

- Fix pi javascript
- Reactivate saint

## DONE or Obsolete

- make path to 'pages' configurable by ENV variable
- create dockerfile for this project
- projects as submodules
- make a docker stack containing all projects
- make a build script (ant ?)
- redirect to subprojects
- use git lfs for pages/*
- make some projects dockerable 
- Schöngddrabern remove image links

### Applications

see helm ingress.ynlp

